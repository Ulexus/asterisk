# How-To: Adding a new library to Asterisk build #
#### Written by: Nir Simionovich, 2017

## General
### Abstract 
Adding an external library to the Asterisk `automake` and `autoconf` is required
when developing new functionality. While in most cases this is not required, 
when adding completely new features - this is sometimes desirable.

Sorry to say, the current Asterisk developer documentation doesn't state 
how a library should be added to the build - and assumes familiarity with
`autoconf` and `automake`. While familiarity with these tools is essential
and beneficial, sometimes it is slightly misplaced. 

This document will explain how to add a new external library to the Asterisk
build mechanism and how to `bootstrap` the `Makefile`.

### Prerequisites 
The document assumes that you have familiarity with C programming and basic
Shell concepts. In addition, you need familiarity with a text editor, `vim` is
the recommended one (but that is my own personal taste).

### Technical Data
N/A

## RPM package prerequisites
N/A

## Adding an external library
For the purpose of this document, I will detail the process I've gone through,
in order to incorporate `libbeanstalkd` to the Asterisk source code. For the 
sake of discussion, `beanstalk` is a *JOB Queue* server, that provides simple,
text based job queuing, with a very light memory and operational foot print.

### Step 1: Add your library to `configure.ac`
In order to include your external library to this file, you will be required to 
add your library in 2 distinct locations - one to indicate its setup, the other
is the library testing.

The sample below shows how this is done:

Circa line: 528
```c
AST_EXT_LIB_SETUP([OSS], [Open Sound System], [oss])
AST_EXT_LIB_SETUP([PGSQL], [PostgreSQL], [postgres])
```

Will now turn to:

```c
AST_EXT_LIB_SETUP([OSS], [Open Sound System], [oss])
AST_EXT_LIB_SETUP([PGSQL], [PostgreSQL], [postgres])
AST_EXT_LIB_SETUP([BEANSTALK], [Beanstalk Job Queue], [beanstalk])
```

*Pay attention to the format of the command*

Circa line: 2173

```c
AST_EXT_LIB_CHECK([BLUETOOTH], [bluetooth], [ba2str], [bluetooth/bluetooth.h])
```

Will now turn to:

```c
AST_EXT_LIB_CHECK([BLUETOOTH], [bluetooth], [ba2str], [bluetooth/bluetooth.h])
AST_EXT_LIB_CHECK([BEANSTALK], [beanstalk], [bs_version], [beanstalk.h])
```

Save your file and exit the text editor.

### Step 2: Add your library to `makeopts.in`

Circa line: 385

```c
SNDFILE_INCLUDE=@SNDFILE_INCLUDE@
SNDFILE_LIB=@SNDFILE_LIB@
```

Will now turn to:

```c
SNDFILE_INCLUDE=@SNDFILE_INCLUDE@
SNDFILE_LIB=@SNDFILE_LIB@

BEANSTALK_INCLUDE=@BEANSTALK_INCLUDE@
BEANSTALK_LIB=@BEANSTALK_LIB@
```

### Step 3: Add your library to `menuselect-deps.in`

Circa line: 1

```c
ALSA=@PBX_ALSA@
BLUETOOTH=@PBX_BLUETOOTH@
```

Will now turn to:

```c
ALSA=@PBX_ALSA@
BLUETOOTH=@PBX_BLUETOOTH@
BEANSTALK=@PBX_BEANSTALK@
```

### Step 4: Run the `bootstrap` script
From within the Asterisk source root directory, execture the `bootstrap` script:

```bash
./bootstrap
```

This will read the newly modified files and create the relevant `Makefile` and 
`configure` scripts, that contain your recently added library. 

# License


This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.