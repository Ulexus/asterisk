# Informational: Asterisk Dialplan Best Practices #
#### Written by: Nir Simionovich, 2017

## General
### Abstract 
The Asterisk diaplan is the core element providing admins and developers the 
capbility of developing complex voice platforms. While the usage of the Asterisk 
dialplan appears to be simplistic at nature, misuse of the dialplan can result 
in unmaintainable code and worse, insecure dialplans that can result in 
significant financial loss.

The purpose of this document is to provide a generalised best-practices document, 
that will include various paradigms for writing and maintaining Asterisk dialplan 
files at ease and securly. We will try to provide information AEL as well as 
dialplan, to make the document as complete as possible.

### Prerequisites 
The document assumes that you already have a working Asterisk installation and 
that you have some familiarity with the `extensions.conf` file and/or 
`extensions.ael` file formats.

### Technical Data
N/A

## RPM package prerequisites
N/A

## Dialplan and AEL best practices

### Syntax is everything
Over the years, the `extensions.conf` file format had evolved. In the early 
years, the format used to look something like this:

```
exten => 1000,1,Answer
exten => 1000,2,Wait(1)
exten => 1000,3,Playback(demo-congrats)
exten => 1000,4,Hangup
```

While the above usage format is still available, it is highly discouraged. The 
above method contains *static* priority numbering, making extension of the above 
dialplan highly problematic. In addition, the usage of consecutive `exten` lines 
makes the code unreadable and hard to maintain, specifically when utilizing 
multiple extensions within the same context and/or extensions file.

The following format is highly recommended:

```
exten => 1000,1,Answer
 same =>      n,Wait(1)
 same =>      n,Playback(demo-congrats)
 same =>      n,Hangup
```

The above format enables several advantages, making life for the developer much 
easier:

* The usage of the `n` priority instead of a number enables insertion of new 
  lines between priorities.
* The usage of the `same` extension keyword enables the developer to create 
  extension blocks that are easily identifiable and visible

Pay careful attention to spacing, it will make your `extensions.conf` file far 
more readable.

### File Separation is Key

### FUNCTIONS, VARIABLES and CHANNEL VARIABLES

Asterisk functions (eg. `TIMEOUT`, `IFTIME`, `CHANNEL`, etc) and various other 
channel variables (`DIALSTATUS`, `DIALEDTIME`, `ANSWERTIME`, etc) all utilize 
capital case. While there is no specific recommendation, we recommend that your 
own variables also be named in CAPITAL letters, only prefixed with a unique 2 
letter prefix.

As an example, let us examine the following diaplan sample:

```
exten => 1000,1,Answer
 same =>      n,Set(personal_timeout=30)
 same =>      n,Set(dial_timeout=120)
 same =>      n,Set(target_dial=SIP/3000)
 same =>      n,Set(TIMEOUT(absolute)=${personal_timeout})
 same =>      n,Dial(${target_dial},${dial_timeout},R)
```

There is nothing wrong with the above dialplan, it will work and it can be 
maintained at ease. However, let us examine a slightly modified version of 
the above dialplan, this time using our paradigm:

```
exten => 1000,1,Answer
 same =>      n,Set(MY_PERSONAL_TIMEOUT=30)
 same =>      n,Set(MY_DIAL_TIMEOUT=120)
 same =>      n,Set(MY_TARGET_SIP=SIP/3000)
 same =>      n,Set(TIMEOUT(absolute)=${MY_PERSONAL_TIMEOUT})
 same =>      n,Dial(${MY_TARGET_SIP},${MY_DIAL_TIMEOUT},R)
```

There is a clear and concise difference between the `absolute` paramater 
specified for the `TIMEOUT` function, while in the previous example, missing 
the `${}` combination is very easy to do and thus, misreading the dialplan. In 
addition, the usage of a prefix (`MY_`) contributes to a simpler logical reading, 
when another person reads the dialplan and immediately understands where a 
`private variable` exists.

### Stop using Macro - use GoSub

### Pay special attention when using Goto

### Using _ and __ constructively

# License
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.